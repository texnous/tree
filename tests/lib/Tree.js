/**
 * @fileoverview Tests of tree structure elements.
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous project team (http://texnous.org) 2017
 * @license LGPL-3.0
 *
 * This unit test is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This unit test is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this unit
 * test; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 */

"use strict";

const Tree = require("../../sources/lib/Tree"); // tree structure elements

let rootNode, node1, node2, node3, node4, node5, node6;
let tree;
let nodesEnumeration;

/**
 * Tests of tree structure elements
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 */
module.exports = {
	"Node": {
		"constructor": test => {
			test.doesNotThrow(() => node1 = new Tree.Node({ }));
			test.doesNotThrow(() => node3 = new Tree.Node({ }));
			test.doesNotThrow(() => node2 = new Tree.Node({parentNode: node3}));
			test.throws(() => new Tree.Node({childNodes: [node2]}));
			test.doesNotThrow(() => node4 = new Tree.Node({}));
			test.doesNotThrow(() => node5 = new Tree.Node({}));
			test.doesNotThrow(() => node6 = new Tree.Node({}));
			test.doesNotThrow(() => rootNode = new Tree.Node({childNodes: [node1, node5]}));
			test.done();
		},
		"insertChildNode": test => {
			test.doesNotThrow(() => rootNode.insertChildNode(node4, 1));
			test.throws(() => rootNode.insertChildNode(node3, 1));
			test.doesNotThrow(() => rootNode.insertChildNode(node6, 1, 2));
			test.done();
		},
		"insertChildSubtree": test => {
			test.doesNotThrow(() => rootNode.insertChildSubtree(node3, 1));
			test.done();
		},
		"childNode": test => {
			test.equal(rootNode.childNode(0), node1);
			test.equal(node6.childNode(1), node5);
			test.equal(node6.childNode(2), null);
			test.equal(node1.childNode(1), null);
			test.done();
		},
		"childNodes": test => {
			test.deepEqual(rootNode.childNodes, [node1, node3, node6]);
			test.deepEqual(node1.childNodes, [ ]);
			test.deepEqual(node3.childNodes, [node2]);
			test.deepEqual(node6.childNodes, [node4, node5]);
			test.done();
		},
		"toString": test => {
			test.equal(
				rootNode.toString(),
				"Tree.Node [ Tree.Node [], Tree.Node [ Tree.Node [] ], Tree.Node [ Tree.Node [], Tree.Node [] ] ]"
			);
			test.equal(
				rootNode.toString(undefined, 2),
				"Tree.Node [ Tree.Node [], Tree.Node [ ... ], Tree.Node [ ... ] ]"
			);
			test.equal(rootNode.toString(true), "Tree.Node {  }");
			test.equal(rootNode.toString(false), "");
			test.equal(
				rootNode.toString("  "),
				"Tree.Node [\n" +
				"  Tree.Node [],\n" +
				"  Tree.Node [\n" +
				"    Tree.Node []\n" +
				"  ],\n" +
				"  Tree.Node [\n" +
				"    Tree.Node [],\n" +
				"    Tree.Node []\n" +
				"  ]\n" +
				"]"
			);
			test.equal(
				rootNode.toString("  ", 2),
				"Tree.Node [\n" +
				"  Tree.Node [],\n" +
				"  Tree.Node [ ... ],\n" +
				"  Tree.Node [ ... ]\n" +
				"]"
			);
			test.done();
		}
	},
	"Tree": {
		"constructor": test => {
			test.throws(() => new Tree());
			test.doesNotThrow(() => tree = new Tree(rootNode));
			test.done();
		},
		"enumerateNodes": test => {
			test.doesNotThrow(() =>	nodesEnumeration = tree.enumerateNodes({
				nodes: true,
				nodeIndices: true,
				keyRoots: true,
				keyRootIndices: true,
				mostLeftChildNodes: true,
				mostLeftChildNodeIndices: true
			}));
			test.deepEqual(nodesEnumeration.nodes, [null, node1, node2, node3, node4, node5, node6, rootNode]);
			test.deepEqual(nodesEnumeration.keyRoots, [node3, node5, node6, rootNode]);
			test.deepEqual(nodesEnumeration.keyRootIndices, [3, 5, 6, 7]);
			test.deepEqual(nodesEnumeration.keyRootIndices, [3, 5, 6, 7]);
			test.deepEqual(nodesEnumeration.mostLeftChildNodeIndices, [0, 1, 2, 2, 4, 5, 4, 1]);
			test.done();
		}
	}
};
